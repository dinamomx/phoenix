

$(function () {

  console.log("ready!");
  var currentSlide;
  // recomendacion
  // nombrar las clases que tienen un acción con un prefijo que sea fácil distinguir
  $('.btn-slide').click(function(e){
    location.hash = "#myPage";
    // asigna el valor de data-target para distinguir que foto mostrar
    var target = $(this).data('target');
    var slideID = target.split('-');
    currentSlide= slideID[1];
    console.log(currentSlide);
    // muestra la modal
    // recomendacion, cambiar el estado de la modal con clases para poder animarlo
    $('#myModal1').css('display','block');
    $('.'+target).addClass('show');
    e.preventDefault();
  })
  $('.btn-slide-change').click(function(e){
    // asigna el valor de data-target para distinguir que foto mostrar
    var target = $(this).data('target');
    var slideID = target.split('-');
    currentSlide= slideID[1];
    console.log(currentSlide);

    //oculta los lides que no se debenmostrar
    $(".mySlides").removeClass('show');
    //muestra el slide que corresponde al target
    $('.'+target).addClass('show');
    e.preventDefault();
  })

  $('.btn-close').click(function () {
    $("#myModal1").css('display','none');
    $(".mySlides").removeClass('show');
  });

  $('.btn-prev').click(function(){  
    currentSlide = parseInt(currentSlide,10) -1;
    console.log(currentSlide);
    if (currentSlide <= 0) {
      console.log('llegaste a cero, tienes que ir al cuatro')
      currentSlide = 2;
    }else{
      $(".mySlides").removeClass('show');
    $('.slide-'+String(currentSlide)).addClass('show');
    }
    
    
  });

  $('.btn-next').click(function(){
    currentSlide = parseInt(currentSlide,10) +1;

    if (currentSlide > 4) {
      console.log('llegaste a cuatro, tienes que ir al 1')
      currentSlide = 3;
    }else{
      $(".mySlides").removeClass('show');
      $('.slide-'+String(currentSlide)).addClass('show');
      // console.log(currentSlide);
    }
    
   
  });



});


//import '../../src/css/style.css';
import Vue from '../../node_modules/vue/dist/vue.common.js';
import FormGeneric from '../FormGeneric.vue';
import {
  CollapseTransition
} from 'vue2-transitions';

console.log(FormGeneric);


Vue.component(FormGeneric.name, FormGeneric);
Vue.component(CollapseTransition.name, CollapseTransition);

// console.log(FormGeneric);

Document.prototype.ready = callback => {
  const stateIsReady = state => state === 'interactive' || state === 'complete';
  if (callback && typeof callback === 'function') {
    if (stateIsReady(document.readyState)) {
      return callback();
    }
    document.addEventListener('DOMContentLoaded', () => {
      if (stateIsReady(document.readyState)) {
        return callback();
      }
    });
  }
};
document.ready(() => {
  const app = new Vue({
    el: '#app',
    data: {
      showTerms: false,
      showModel: {
        sportage: false,
        sorento: false
      }
    }
  });

  window.$app = app;
});





$(function () {
  $('.navbar-toggle').click(function () {
    $('.navbar-nav').toggleClass('slide-in');
    $('.side-body').toggleClass('body-slide-in');
    $('#search').removeClass('in').addClass('collapse').slideUp(200);

    /// uncomment code for absolute positioning tweek see top comment in css
    //$('.absolute-wrapper').toggleClass('slide-in');

  });

  // Remove menu for searching
  $('#search-trigger').click(function () {
    $('.navbar-nav').removeClass('slide-in');
    $('.side-body').removeClass('body-slide-in');

    /// uncomment code for absolute positioning tweek see top comment in css
    //$('.absolute-wrapper').removeClass('slide-in');

  });
});



$(document).ready(function () {



  // Initialize Tooltip
  $('[data-toggle="tooltip"]').tooltip();

  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function (event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {

      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function () {



        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });

    } // End if
    $('.navbar-nav').toggleClass('slide-in');
    $('.side-body').toggleClass('body-slide-in');
  });

})